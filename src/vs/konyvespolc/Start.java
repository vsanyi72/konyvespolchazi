package vs.konyvespolc;

public class Start {

	public static void main(String[] args) {
		Konyv konyv1 = new Konyv("123", "Jókai", "Arany ember");
		Konyv konyv2 = new Konyv("456", "Petőfi", "Összes ...");
		Konyv konyv3 = new Konyv("789", "Móricz", "Tóth atyafiak");
		Polc polc = new Polc();
		polc.addKonyv(konyv1);
		polc.addKonyv(konyv2);
		polc.addKonyv(konyv3);
		System.out.println("A polc kiirása:");
		System.out.println(polc);
		polc.removeKonyv(konyv2);
		System.out.println(polc);
		System.out.println();
		System.out.println("Itt jön a vásárló kiírása:");
		Vasarlo vasarlo = new Vasarlo();
		vasarlo.setEmail("Kovacs@gmail.com");
		vasarlo.setNev("Kovacs Jozsef");
		vasarlo.setPolcok(polc);
		System.out.println("A vásárló adatai : "+vasarlo);
		vasarlo.getPolcok().removeKonyv(konyv1);
		vasarlo.getPolcok().addKonyv(konyv2);
		vasarlo.setPolcok(polc);
		System.out.println("A vásárló polc módosítása után : "+ vasarlo);

	}

}
