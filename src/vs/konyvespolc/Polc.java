package vs.konyvespolc;

public class Polc {
	private Konyv[] konyvek;

	public Polc() {
		super();
		this.konyvek = new Konyv[10];

	}

	public Konyv[] getKonyvek() {
		return konyvek;
	}

	public void setKonyvek(Konyv[] konyvek) {
		this.konyvek = konyvek;
	}

	public void addKonyv(Konyv konyv) {
		int i;
		for (i = 0; i < this.konyvek.length; i++) {
			if (this.konyvek[i] == null) {
				this.konyvek[i] = konyv;
				break;
			}
		}
		if (i == this.konyvek.length) {
			System.out.println("Megtelt a polc");
		}
	}

	public int removeKonyv(Konyv konyv) {
		for (int i = 0; i < this.konyvek.length; i++) {
			if (this.konyvek[i].equals(konyv)) {
				this.konyvek[i] = null;
				return i;
			}
		}
		return -1;

	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i <  this.konyvek.length; i++) {
			sb.append(this.konyvek[i]);
			if(i<this.konyvek.length-1) {
				sb.append(",");
			}
		}
		return sb.toString();

	}

}
