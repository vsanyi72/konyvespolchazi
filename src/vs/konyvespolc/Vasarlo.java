package vs.konyvespolc;

public class Vasarlo {
	private String nev;
	private String email;
	private Polc polcok;

	public Vasarlo(String nev, String email) {
		super();
		this.nev = nev;
		this.email = email;
	}

	public Vasarlo() {
		super();
	}

	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
	public Polc getPolcok() {
		return polcok;
	}

	public void setPolcok(Polc polcok) {
		this.polcok = polcok;
	}

	
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.nev);
		sb.append(", ");
		sb.append(this.email);
		sb.append(", ");
		sb.append(this.getPolcok());

		return sb.toString();
	}

}
