package vs.konyvespolc;

public class Konyv 
{
	private String isbn;
	private String szerzo;
	private String cim;
	public Konyv(String isbn, String szerzo, String cim) {
		super();
		this.isbn = isbn;
		this.szerzo = szerzo;
		this.cim = cim;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public String getSzerzo() {
		return szerzo;
	}
	public void setSzerzo(String szerzo) {
		this.szerzo = szerzo;
	}
	public String getCim() {
		return cim;
	}
	public void setCim(String cim) {
		this.cim = cim;
	}
	public String toString() {
		return new StringBuilder(this.isbn)
				.append(",")
				.append(this.szerzo)
				.append(",")
				.append(this.cim)
				.append(System.lineSeparator())
				.toString();

	}
	
}
